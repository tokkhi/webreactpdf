import React from "react";
import ReactDOM from "react-dom";
import Home from './component/Home';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import { Container,Navbar,Nav } from "react-bootstrap";

function App() {
  
  return (
    
    <div>
       <Navbar className="navbar-custom">
            <Navbar.Brand href="/home"><img src="./images/logo.png" alt="logo" /></Navbar.Brand>
            <Nav className="mr-auto"> 
               
                {/* <Nav.Link href="#features">Features</Nav.Link>
                <Nav.Link href="#pricing">Pricing</Nav.Link> */}
            </Nav>
            </Navbar>
  <Container fluid="true">
      <Router>
      <Switch>
        <Route path="/" render={(json) => <Home {...json} isAuthed={true} />}/>
        {/* <Redirect from="/" to="home" /> */}
      </Switch>
      </Router>   
  </Container>
    </div>
  );
  
}


ReactDOM.render(
    <App />,
  document.getElementById("root")
);
// ReactPDF.render(<MyDocument />, '${__dirname}/example.pdf');
export default App;